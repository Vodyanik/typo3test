<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

call_user_func(function() {

    $extensionKey = 'blog';

    \FluidTYPO3\Flux\Core::registerProviderExtensionKey('Test.Blog', 'Page');
    \FluidTYPO3\Flux\Core::registerProviderExtensionKey('Test.Blog', 'Content');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extensionKey, 'Configuration/TypoScript', 'Blog extension');
});