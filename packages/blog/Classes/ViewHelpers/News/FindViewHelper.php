<?php

namespace Test\Blog\ViewHelpers\News;

use GeorgRinger\News\Domain\Repository\NewsRepository;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class FindViewHelper extends AbstractViewHelper
{
    protected $newsRepository;

    public function __construct(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    public function initializeArguments()
    {
        $this->registerArgument('newsUid', 'integer', 'News Uid');
    }

    public function render()
    {
        return $this->newsRepository->findByUid($this->arguments['newsUid']);
    }
}