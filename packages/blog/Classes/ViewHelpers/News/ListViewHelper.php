<?php

namespace Test\Blog\ViewHelpers\News;

use GeorgRinger\News\Domain\Repository\NewsRepository;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class ListViewHelper extends AbstractViewHelper
{
    protected $newsRepository;

    public function __construct(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    public function render()
    {
        return $this->getNews();
    }

    protected function getNews()
    {
        $query = $this->newsRepository->createQuery();
        $query->getQuerySettings()->setStoragePageIds([5]);
        $result = $query->execute();

        $news = [];
        foreach ($result as $item) {
            $news[$item->getUid()] = $item->getTitle();
        }
        return $news;
    }
}